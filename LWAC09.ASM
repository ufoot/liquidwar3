; routines de calcul de Liquid War */



.286

DOSSEG

.MODEL LARGE

.DATA

wallgrad	equ	48000
fargrad		equ	16000

tabgoto2	dw	256	dup	(?)  ;ces tab doivent � cons�cutifs


howmany		dw	6	dup	(?)



.CODE

public _stillhowmany
public _fillgoto
public _boostgradient 			  ;actualise un gradient
public _peekgradient
public _pokegradient
public _pokeall
public _peekwhatis
public _clearmen
public _putman
public _boostsquare

;*** "compactage" maison **************************************

;_compact58	proc	far
;
;sframe		struc
;
;bp8		dw	?
;ret_adr8        dd	?
;adr5		dw	?
;adr8		dw	?
;
;sframe		ends
;
;frame 		equ	[bp-bp8]
;
;		push	bp
;		mov	bp,sp
;
;		push	di
;		push	si
;
;		mov	di,frame.adr5
;		mov     si,frame.adr8
;
;		xor	bh,bh
;		xor	cx,cx
;		xor	dl,dl
;
;		mov	ax,ds:[si]
;		mov	bl,al
;		and	bl,00011111b
;
;		and	ax,0001111100000000b
;		shr	ax,3
;		or	bx,ax
;
;		mov	ax,ds:[si+2]
;
;		;etc...
;
;		pop	si
;		pop	di
;
;		pop	bp
;
;		retf
;
;_compact58	endp
;
;***renvoie le nombre de carr�s appartenants � une �quipe donn�e

_stillhowmany	proc	far

		pop	ax
		pop	dx
		pop	bx

		push	bx
		push	dx
		push	ax

		shl	bx,1
		add	bx,offset howmany
		mov	ax,[bx]

		retf

_stillhowmany	endp

;**** rempli les tableaux de goto ***************************

_fillgoto	proc	far

		mov     cx,256
		mov	bx,offset tabgoto2
		xor	al,al
		mov	dx,offset fonction1bis ;pour eviter les bugges
bcfigo1:


		test	al,00000010b
		je      stfigo1
		mov	dx,offset fonction2bis
		jmp	stfigo8

stfigo1:        test	al,00010000b
		je	stfigo2
		mov	dx,offset fonction5bis
		jmp	stfigo8

stfigo2:        test	al,01000000b
		je	stfigo3
		mov	dx,offset fonction7bis
		jmp	stfigo8

stfigo3:        test	al,00001000b
		je	stfigo4
		mov	dx,offset fonction4bis
		jmp	stfigo8

stfigo4:        test	al,00000100b
		je	stfigo5
		mov	dx,offset fonction3bis
		jmp	stfigo8

stfigo5:        test	al,10000000b
		je	stfigo6
		mov	dx,offset fonction8bis
		jmp	stfigo8

stfigo6:        test	al,00100000b
		je	stfigo7
		mov	dx,offset fonction6bis
		jmp	stfigo8

stfigo7:	test	al,00000001b
		je	stfigo8
		mov	dx,offset fonction1bis

stfigo8:        mov     [bx],dx

		inc	al
		inc	bx
		inc	bx
		loop	bcfigo1

		retf

_fillgoto	endp


;**** actualise un des 6 gradients ************************

_boostgradient	proc	far

sframe		struc

bp0		dw	?
ret_adr0        dd	?
sizex		dw	?
sizey		dw	?
numgrad		dw	?
dir		dw	?   ;0=droite 2=bas 4=gauche 6=haut
		dw	?
&tab		dw	?

sframe		ends

frame		equ	[bp-bp0]

		push	bp
		mov	bp,sp

		push	ds
		push	es
		push	di
		push	si

		and 	frame.dir,0000000000000111b

		mov	ax,frame.numgrad
		inc	ax
		inc	ax
		shl	ax,1
		mov	si,ax         ;si contient la valeur de
				      ;de d�part de bx, cad 1
				      ;offset li� au num�ro du
				      ;gradient


		mov	di,frame.sizex
				       ;di contient sizex



		mov	ax,frame.dir
		test	al,00000100b       ;direction <4 ?
		je	stboost1
		jmp	stboost11

	;traitement "croissant"
stboost1:

		mov	bx,frame.&tab
		add	bx,di
		inc	bx
		mov	ds,bx              ;ds:point � modifier


		test	al,00000010b        ;direction <2 ?
		jne	stboost4
		dec	bx                ;1 cran � gauche
stboost4:	or	al,al             ;direction =0 ?
		je	stboost5
		sub	bx,di             ;1 cran en haut
		cmp	al,3
		jne	stboost5
		inc	bx                ;1 cran � droite
stboost5:	mov	es,bx             ;es:source test�e


		mov	cx,frame.sizey
		dec	cx
		dec	cx

bcboost1:
		push	cx

		mov	cx,di
		dec	cx
		dec	cx

		mov	bx,si

bcboost2:
		mov	ax,ds:[bx]
		inc	ax
		jl	stboost6
		mov	dx,es:[bx]
		inc	dx
		cmp	ax,dx
		jb      stboost7
		mov	ax,dx
stboost7:    	mov	ds:[bx],ax
stboost6:       add	bx,16

		loop	bcboost2


		mov	ax,ds
		add	ax,di
		mov	ds,ax

		mov	ax,es
		add	ax,di
		mov	es,ax




		pop	cx
		loop	bcboost1



		jmp 	stboost3

	;traitement "d�croissant"
stboost11:

		push	ax

		mov	ax,frame.sizey
		mul	frame.sizex
		mov	bx,frame.&tab
		add	bx,ax

		pop	ax

		sub	bx,di
		sub	bx,di
		mov	ds,bx		;ds:pt � modifier


		and	al,00000011b
		test	al,00000010b        ;direction <2 ?
		jne	stboost14
		inc	bx                ;1 cran � droite
stboost14:	or	al,al             ;direction =0 ?
		je	stboost15
		add	bx,di             ;1 cran en bas
		cmp	al,3
		jne	stboost15
		dec	bx                ;1 cran � gauche
stboost15:	mov	es,bx             ;es:source test�e


		mov	ax,di
		shl	ax,4
		add     si,ax
		sub	si,32            ;on change si car traitement d�c

		mov	cx,frame.sizey
		dec	cx
		dec	cx
bcboost11:
		push	cx

		mov	cx,di
		dec	cx
		dec	cx

		mov	bx,si
bcboost12:
		mov	ax,ds:[bx]
		inc	ax
		jl	stboost16
		mov	dx,es:[bx]
		inc	dx
		cmp	ax,dx
		jb      stboost17
		mov	ax,dx
stboost17:  	mov	ds:[bx],ax
stboost16:      sub	bx,16

		loop	bcboost12

		mov	ax,ds
		sub	ax,di
		mov	ds,ax

		mov	ax,es
		sub	ax,di
		mov	es,ax


		pop	cx
		loop	bcboost11



	;sortie
stboost3:

		pop	si
		pop	di
		pop	es
		pop	ds

		pop	bp
		retf

_boostgradient	endp


;peek une valeur de gradient pour l'utiliser en C (pour v�rifs etc...)

_peekgradient	proc	far

sframe		struc

bp1		dw	?
ret_adr1	dd	?
item1		dw	?
num1		dw	?
		dw	?
seg1		dw	?

sframe		ends

frame		equ	[bp-bp1]

		push	bp
		mov	bp,sp

		push	es

		mov	ax,frame.seg1
		add	ax,frame.item1
		mov	es,ax

		mov     bx,frame.num1
		inc	bx
		inc	bx
		shl	bx,1

		mov     ax,es:[bx]

		pop	es

		pop	bp
		retf

_peekgradient	endp


;poke une valeur de gradient depuis le C

_pokegradient	proc	far

sframe		struc

bp2		dw	?
ret_adr2	dd	?
val2		dw	?
item2		dw	?
num2		dw	?
		dw	?
seg2		dw	?

sframe		ends

frame		equ	[bp-bp2]

		push	bp
		mov	bp,sp

		push	es

		mov	ax,frame.seg2
		add	ax,frame.item2
		mov	es,ax

		mov     bx,frame.num2
		inc	bx
		inc	bx
		shl	bx,1

		mov	ax,frame.val2
		mov     es:[bx],ax

		pop	es

		pop	bp
		retf

_pokegradient	endp


;poke les 16 octets en fc de la couleur du grob (initialisation)

_pokeall	proc	far

sframe		struc

bp3		dw	?
ret_adr3	dd	?
val3		dw	?
item3		dw	?
		dw	?
seg3		dw	?

sframe		ends

frame		equ	[bp-bp3]

		push	bp
		mov	bp,sp

		push	es

		mov	ax,frame.seg3
		add	ax,frame.item3
		mov	es,ax

		mov	ax,frame.val3
		mov	bl,al
		shr	bl,4
		cmp	bl,3
		jne	stpokeall1

		mov	dx,fargrad
		jmp	stpokeall2
stpokeall1:
		mov	dx,wallgrad
stpokeall2:
		xor	bx,bx
		xor     ah,ah
		xchg	al,ah

		mov	es:[bx],ax

		mov	bx,2

		mov	es:[bx],ax

		mov	cx,6
bcpokeall1:	inc	bx
		inc	bx
		mov     es:[bx],dx
		loop	bcpokeall1

		pop	es

		pop	bp
		retf

_pokeall	endp

;****** retourne ce qu'il y a sur la case***********************


_peekwhatis	proc	far


sframe	struc

bp4		dw	?
ret_adr4	dd	?
x4		dw	?
y4		dw	?
sizex4		dw	?
		dw	?
&tab4		dw	?

sframe	ends

frame		equ	[bp-bp4]

		push	bp
		mov	bp,sp

		push	es
		push	di

		mov	bx,frame.&tab4
		mov	ax,frame.y4
		mul	frame.sizex4
		add	ax,bx
		add	ax,frame.x4
		mov	es,ax

		xor	di,di

		mov	ax,es:[di]

		pop	di
		pop	es


		pop	bp
		retf

_peekwhatis	endp

;vide la case correspondante

_clearmen	proc	far

sframe  struc

bp5a		dw	?
ret_adr5a	dd	?
x5a		dw	?
y5a		dw	?
		dw	?
&tab5a		dw	?

sframe		ends

frame		equ	[bp-bp5a]

		push	bp
		mov	bp,sp

		cld

		push	di
		push 	si
		push	es


		mov	bx,frame.&tab5a
		mov	es,bx

		mov	ax,frame.y5a
		mul	frame.x5a
		mov	cx,ax

		xor	si,si

bcclm1:

		mov	di,2
		mov	ax,es:[di]
		mov	es:[si],ax

		and	ax,1111000000000000b
		cmp	ax,0010000000000000b
		jne	stclm2


		mov	ax,wallgrad

		jmp	stclm1
stclm2:

		mov	ax,fargrad
stclm1:

		mov	di,4
		stosw
		stosw
		stosw
		stosw
		stosw
		stosw

		mov	ax,es
		inc	ax
		mov	es,ax
		loop	bcclm1

		pop	es
		pop	si
		pop	di




		pop	bp

		retf

_clearmen	endp

;ajoute 1 mec


_putman		proc	far

sframe	struc

bp5		dw	?
ret_adr5	dd	?
x5		dw	?
y5		dw	?
sizex5		dw	?
c5		dw	?
health5		dw	?        ;0=mort 8191=la patate
		dw	?
&tab5		dw	?

sframe	ends

frame		equ	[bp-bp5]

		push	bp
		mov	bp,sp

		push	di
		push	es

		mov	bx,frame.&tab5
		mov	ax,frame.y5
		mul	frame.sizex5
		add	ax,bx
		add	ax,frame.x5
		mov	es,ax

		xor	di,di

		mov	ax,frame.c5
		inc	al
		inc	al
		shl	ax,13
		add	ax,frame.health5
		mov	es:[di],ax

		pop	es
		pop	di

		pop	bp
		retf

_putman	endp


;*fait bouger les carr�s

_boostsquare	proc	far

sframe		struc

bp6		dw	?
ret_adr6	dd	?
sizex6		dw	?
sizey6		dw	?
updown6		dw	?
recharge6       dw	?
semidecharge6	dw	?
decharge6	dw	?
		dw	?
&tab6 		dw	?

sframe		ends

frame		equ	[bp-bp0]

		push	bp
		mov	bp,sp

		push	es
		push	di
		push	si

		call	refreshandcount

		mov	ax,frame.updown6
		test	ax,1
		jne	stbsqp1

		call	downsquare
		jmp	stbsqp2
stbsqp1:
		call	upsquare
stbsqp2:

		pop	si
		pop	di
		pop	es

		pop	bp
		retf

_boostsquare	endp


;rafraichit la "2eme" page de calcul et compte les survivants

refreshandcount	proc	near

		mov	ax,frame.&tab6
		add	ax,frame.sizex6
		inc	ax
		mov	es,ax

		mov	bx,offset howmany
		xor	ax,ax
		mov	[bx],ax
		mov	[bx+2],ax
		mov	[bx+4],ax
		mov	[bx+6],ax
		mov	[bx+8],ax
		mov	[bx+10],ax

		xor	di,di
		mov	si,2

		mov	cx,frame.sizey6
		dec	cx
		dec	cx

bcrfact1:       push	cx

		mov	cx,frame.sizex6
		dec	cx
		dec	cx

bcrfact2:
		mov	ax,es:[di]
		cmp	ah,64
		jb	strfact1


		mov	bl,ah
		shr	bl,5
		dec	bl
		dec	bl
		shl	bl,1
		xor	bh,bh
		add	bx,offset howmany
		inc     word ptr ds:[bx]

strfact1:
		mov	es:[si],ah

		mov	ax,es
		inc	ax
		mov	es,ax

		loop	bcrfact2

		mov	ax,es
		inc	ax
		inc	ax
		mov	es,ax

		pop	cx
		loop	bcrfact1

		ret

refreshandcount	endp



;fait bouger les carr�s, balayage vers le bas

downsquare	proc	near



		mov	ax,frame.&tab6
		mov	es,ax    ;es=point sup�rieur gauche

		mov	si,frame.sizex6
		shl	si,4     ;si � conserver, contient sizex*16

		mov	cx,frame.sizey6
		dec	cx
		dec	cx

bcdsq1:		push	cx

		mov	cx,frame.sizex6
		dec	cx
		dec	cx

bcdsq2:         push	cx


		mov	di,si
		add	di,18
		mov	bh,es:[di]
		cmp	bh,64
		jb	stdsq2

		call	onesquare

stdsq2:

		mov	ax,es
		inc	ax
		mov	es,ax

		pop	cx
		loop	bcdsq2

		mov	ax,es
		inc	ax
		inc	ax
		mov	es,ax

		pop	cx
		loop	bcdsq1


		ret

downsquare	endp



;idem mais balayage vers le haut

upsquare	proc	near


		mov	ax,frame.sizey6
		dec	ax
		dec	ax
		mul	frame.sizex6
		dec	ax
		dec	ax
		dec	ax
		add	ax,frame.&tab6

		mov	es,ax    ;es=point sup�rieur gauche

		mov	si,frame.sizex6
		shl	si,4     ;si � conserver, contient sizex*16

		mov	cx,frame.sizey6
		dec	cx
		dec	cx

bcusq1:		push	cx

		mov	cx,frame.sizex6
		dec	cx
		dec	cx

bcusq2:         push	cx


		mov	di,si
		add	di,18
		mov	bh,es:[di]
		cmp	bh,64
		jb	stusq2

		call	onesquare

stusq2:

		mov	ax,es
		dec	ax
		mov	es,ax

		pop	cx
		loop	bcusq2

		mov	ax,es
		dec	ax
		dec	ax
		mov	es,ax

		pop	cx
		loop	bcusq1


		ret


		ret

upsquare	endp


;bouge	un seul	carr�

onesquare	proc	near



		xor	dx,dx	;	dh=grad<	,	dl=grad=
		xor	cx,cx   ;	ch=ami		,	cl=ennemi
		dec	cx      ;on initialise � FFFFh

		shr	bx,13
		shl	bx,1    ;on choisit la page de gradient � adresser

;test des gradients

		dec	di
		dec	di
		add     di,bx

		mov	ax,es:[di]                   ;point courant

		mov	di,bx			;point haut gauche


		cmp	ax,es:[di]
		jb	stosga1
		jne	stosgb1
		or	dl,1
stosgb1:	or	dh,1
stosga1:        ror	dx,1

		add	di,16			;point haut

		cmp	ax,es:[di]
		jb	stosga2
		jne	stosgb2
		or	dl,1
stosgb2:	or	dh,1
stosga2:        ror	dx,1

		add	di,16			;point haut droite

		cmp	ax,es:[di]
		jb	stosga3
		jne	stosgb3
		or	dl,1
stosgb3:	or	dh,1
stosga3:        ror	dx,1

		mov	di,bx			;point gauche
		add	di,si

		cmp	ax,es:[di]
		jb	stosga4
		jne	stosgb4
		or	dl,1
stosgb4:	or	dh,1
stosga4:        ror	dx,1

		add	di,32			;point droite

		cmp	ax,es:[di]
		jb	stosga5
		jne	stosgb5
		or	dl,1
stosgb5:	or	dh,1
stosga5:        ror	dx,1

		add	di,si
		sub	di,32				;point bas gauche

		cmp	ax,es:[di]
		jb	stosga6
		jne	stosgb6
		or	dl,1
stosgb6:	or	dh,1
stosga6:        ror	dx,1

		add	di,16			;point bas

		cmp	ax,es:[di]
		jb	stosga7
		jne	stosgb7
		or	dl,1
stosgb7:	or	dh,1
stosga7:        ror	dx,1

		add	di,16			;point bas droite

		cmp	ax,es:[di]
		jb	stosga8
		jne	stosgb8
		or	dl,1
stosgb8:	or	dh,1
stosga8:        ror	dx,1

		xor	dh,dl		;transforme <= en <



		shl	bx,12		;bx pret � etre compar�

		xor	di,di		;point haut gauche

		mov	ax,es:[di]
		cmp	ah,64
		jb	stosta1
		and	ah,11100000b
		cmp	ah,bh
		je	stostb1
		and	cl,11111110b
		jmp	stosta1
stostb1:        and	ch,11111110b
stosta1:        ror	cx,1

		mov	di,16		;point haut

		mov	ax,es:[di]
		cmp	ah,64
		jb	stosta2
		and	ah,11100000b
		cmp	ah,bh
		je	stostb2
		and	cl,11111110b
		jmp	stosta2
stostb2:        and	ch,11111110b
stosta2:        ror	cx,1

		mov	di,32		;point haut droite

		mov	ax,es:[di]
		cmp	ah,64
		jb	stosta3
		and	ah,11100000b
		cmp	ah,bh
		je	stostb3
		and	cl,11111110b
		jmp	stosta3
stostb3:        and	ch,11111110b
stosta3:        ror	cx,1

		mov	di,si
					;point gauche

		mov	ax,es:[di]
		cmp	ah,64
		jb	stosta4
		and	ah,11100000b
		cmp	ah,bh
		je	stostb4
		and	cl,11111110b
		jmp	stosta4
stostb4:        and	ch,11111110b
stosta4:        ror	cx,1

		add	di,32		;point  droite

		mov	ax,es:[di]
		cmp	ah,64
		jb	stosta5
		and	ah,11100000b
		cmp	ah,bh
		je	stostb5
		and	cl,11111110b
		jmp	stosta5
stostb5:        and	ch,11111110b
stosta5:        ror	cx,1

		mov	di,si
		shl	di,1		;point bas gauche

		mov	ax,es:[di]
		cmp	ah,64
		jb	stosta6
		and	ah,11100000b
		cmp	ah,bh
		je	stostb6
		and	cl,11111110b
		jmp	stosta6
stostb6:        and	ch,11111110b
stosta6:        ror	cx,1


		add	di,16		;point bas

		mov	ax,es:[di]
		cmp	ah,64
		jb	stosta7
		and	ah,11100000b
		cmp	ah,bh
		je	stostb7
		and	cl,11111110b
		jmp	stosta7
stostb7:        and	ch,11111110b
stosta7:        ror	cx,1

		add	di,16		;point bas droite

		mov	ax,es:[di]
		cmp	ah,64
		jb	stosta8
		and	ah,11100000b
		cmp	ah,bh
		je	stostb8
		and	cl,11111110b
		jmp	stosta8
stostb8:        and	ch,11111110b
stosta8:        ror	cx,1


		or	dx,dx           ;tous les grad>
		je	stosex1

		or	dh,dh           ;tous les grad=
		je	stosex2

		mov	bl,dh
		and     bl,cl
		and	bl,ch           ;libre ?
		je	stosex3

		call	peacedep
		jmp	stosex1

stosex3:        mov	bl,dh
		and	bl,cl           ;ennemi ?
		je	stosex2

		call	scrontch
		jmp	stosex1

stosex2:        mov	bl,dl
		and	bl,cl
		and	bl,ch           ;libre ?
		je	stosex4

		call	peacedep
		jmp	stosex1

stosex4:        mov	bl,dl
		and	bl,cl           ;ennemi ?
		je	stosex5

		call	semiscrontch
		jmp	stosex1

stosex5:        mov	bl,dh
		and	bl,ch           ;ami ?
		je	stosex6

		call	hospital
		jmp	stosex1

stosex6:        mov	bl,dl
		and	bl,ch
		je	stosex1

		call	hospital


stosex1:

		ret

onesquare	endp

;** action �l�mentaire: d�placement "peacefull"

peacedep	proc	near

		mov	di,si
		add	di,16		;point central
		mov	ax,es:[di]      ;caracteristiques joueur

		xor	bh,bh
		shl	bx,1
		add	bx,offset tabgoto2

		call	word ptr [bx]

		mov	es:[di],ax
		mov	di,si
		add	di,18
		mov	ax,es:[di]
		dec	di
		dec	di
		mov	es:[di],ax


		ret

peacedep	endp

;** action �l�mentaire: attaque

scrontch	proc	near



		xor	bh,bh
		shl	bx,1
		add	bx,offset tabgoto2

		call	word ptr [bx]
		mov	cx,di

		mov	ax,es:[di]
		mov	dh,ah

		and	ah,00011111b
		sub	ax,frame.decharge6
		jb	stscro1

		and	dh,11100000b
		or	ah,dh
		mov	es:[di],ax

		ret


stscro1:	mov	di,16
		add	di,si
		mov	ax,es:[di]
		and	ah,11100000b
		mov	dx,frame.decharge6
		shl	dx,1
		or	ax,dx
		mov	di,cx
		mov	es:[di],ax


		ret


scrontch	endp


;** action �l�mentaire: attaque douce...

semiscrontch	proc	near



		xor	bh,bh
		shl	bx,1
		add	bx,offset tabgoto2

		call	word ptr [bx]
		mov	cx,di

		mov	ax,es:[di]
		mov	dh,ah


		and	ah,00011111b
		sub	ax,frame.semidecharge6
		jb	stsescro1

		and	dh,11100000b
		or	ah,dh
		mov	es:[di],ax

		ret


stsescro1:	mov	di,16
		add	di,si
		mov	ax,es:[di]
		and	ah,11100000b
		mov	dx,frame.semidecharge6
		shl	dx,1
		or	ax,dx
		mov	di,cx
		mov	es:[di],ax


		ret


semiscrontch	endp



;** action �l�mentaire: recharge

hospital	proc	near



		xor	bh,bh
		shl	bx,1
		add	bx,offset tabgoto2

		call	word ptr [bx]

		mov	ax,es:[di]
		mov	dh,ah

		and	ah,00011111b
		add	ax,frame.recharge6
		test	ah,00100000b
		jne	sthosp1

		and	dh,11100000b
		or	ah,dh
		mov	es:[di],ax

		ret


sthosp1:
		and	dh,11100000b
		mov	ax,0001111111111111b
		or	ah,dh
		mov	es:[di],ax


		ret


hospital	endp


;**les 16 fonctions qui suivent chargent di avec 1 offset % dir

fonction1bis	proc	near

		xor	di,di		;haut gauche

		ret

fonction1bis	endp

fonction2bis	proc	near

		mov	di,16

		ret

fonction2bis	endp

fonction3bis	proc	near

		mov	di,32		;haut droite

		ret

fonction3bis	endp

fonction4bis	proc	near

		mov	di,si		;gauche

		ret

fonction4bis	endp

fonction5bis	proc	near

		mov	di,si		;droite
		add	di,32

		ret

fonction5bis	endp

fonction6bis	proc	near

		mov	di,si		;bas gauche
		add	di,si

		ret

fonction6bis	endp

fonction7bis	proc	near

		mov	di,si		;bas
		add	di,si
		add	di,16

		ret

fonction7bis	endp

fonction8bis	proc	near

		mov	di,32		;bas	droite
		add	di,si
		add	di,si

		ret

fonction8bis	endp




end