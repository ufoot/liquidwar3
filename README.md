Liquid War 3
============

![Liquid War 3 splash screen](https://gitlab.com/ufoot/liquidwar3/raw/master/LW3SPLSH.JPG)

Liquid War is a unique multiplayer wargame.

This is the source code for [legacy version 3](https://ufoot.org/liquidwar/legacy/v3).
It is a DOS only program, from the 20th century, the result of a student summer hack.

If you want a real piece of software, rather consider
[Liquid War 5](https://ufoot.org/liquidwar/v5) or
[Liquid War 6](https://www.gnu.org/software/liquidwar6).

Status
------

This is really abandonware, I'm really not maintaining this any more, no living
entity on this planet can figure out how this old machinery works, really.
However it is nice to have this piece of software public and shared. Honestly, a lot
of things written there are naive, half-broken pieces of code, but this is how
I got started. View this as a historical piece of code, good for a museum,
my very own personnal collection of "you should not do this" stuff.

Among other things, it has:

* totally insane and cryptic file naming (aggree, 8.3 char filenames in DOS doesn't help)
* irrelevant [French comments](https://gitlab.com/ufoot/liquidwar3/blob/84c0530baf6176a5c680b1300031b0d3088728e6/LWAC09.ASM#L112) with no added value
* ugly hacky hacks, the worst of them being "auto-programming", poking constants in code segments at run time to save registers
* ...

Still, that thing... works. I was still recently able to run it on a [DOSBox](https://www.dosbox.com/) emulator.

Documentation
-------------

You kidding? Doc for this? Serious?

Authors
-------

* Christian Mauduit <ufoot@ufoot.org> : main developper, project
  maintainer.

License
-------

This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to [unlicense.org](https://unlicense.org)
