
/*routines concernant la gestion clavier etc... de bouboule */

#include "lwhd09.h"

#include <stdio.h>
#include <stdlib.h>


/*-- Scan codes de diff�rentes touches -------------------------------*/
/*-- From la BIBLE ---------------------------------------------------*/

#define SC_ESC             0x01
#define SC_1               0x02
#define SC_2               0x03
#define SC_3               0x04
#define SC_4               0x05
#define SC_5               0x06
#define SC_6               0x07
#define SC_7               0x08
#define SC_8               0x09
#define SC_9               0x0A
#define SC_0               0x0B
#define SC_SCHARFES_S      0x0C
#define SC_APOSTROPH       0x0D
#define SC_BACKSPACE       0x0E
#define SC_TAB             0x0F
#define SC_Q               0x10
#define SC_W               0x11
#define SC_E               0x12
#define SC_R               0x13
#define SC_T               0x14
#define SC_Z               0x15
#define SC_U               0x16
#define SC_I               0x17
#define SC_O               0x18
#define SC_P               0x19
#define SC_UE              0x1A
#define SC_PLUS            0x1B
#define SC_RETURN          0x1C
#define SC_CONTROL         0x1D
#define SC_A               0x1E
#define SC_S               0x1F
#define SC_D               0x20
#define SC_F               0x21
#define SC_G               0x22
#define SC_H               0x23
#define SC_J               0x24
#define SC_K               0x25
#define SC_L               0x26
#define SC_OE              0x27
#define SC_AE              0x28
#define SC_PLUSGRAND       0x29
#define SC_SHIFT_GAUCHE    0x2A
#define SC_FIS             0x2B
#define SC_Y               0x2C
#define SC_X               0x2D
#define SC_C               0x2E
#define SC_V               0x2F
#define SC_B               0x30
#define SC_N               0x31
#define SC_M               0x32
#define SC_VIRGULE         0x33
#define SC_POINT           0x34
#define SC_TIRET           0x35
#define SC_SHIFT_DROIT     0x36
#define SC_PRINT_SCREEN    0x37
#define SC_ALT             0x38
#define SC_SPACE           0x39
#define SC_CAPS            0x3A
#define SC_F1              0x3B
#define SC_F2              0x3C
#define SC_F3              0x3D
#define SC_F4              0x3E
#define SC_F5              0x3F
#define SC_F6              0x40
#define SC_F7              0x41
#define SC_F8              0x42
#define SC_F9              0x43
#define SC_F10             0x44
#define SC_NUM_LOCK        0x45
#define SC_SCROLL_LOCK     0x46
#define SC_CURSOR_HOME     0x47
#define SC_CURSOR_UP       0x48
#define SC_CURSOR_PG_UP    0x49
#define SC_NUM_MOINS       0x4A
#define SC_CURSOR_LEFT     0x4B
#define SC_NUM_5           0x4C
#define SC_CURSOR_RIGHT    0x4D
#define SC_NUM_PLUS        0x4E
#define SC_CURSOR_END      0x4F
#define SC_CURSOR_DOWN     0x50
#define SC_CURSOR_PG_DOWN  0x51
#define SC_INSERT          0x52
#define SC_DELETE          0x53
#define SC_SYS_REQUEST     0x54
#define SC_F11             0x57
#define SC_F12             0x58
#define SC_NOKEY           0x80        /* Pas de touche additionnelle */




/*----------Sort si on a appuy� sur ESC----------------------------------*/

void outesc(void)

{
    if (hasbeenkey(1))
       exit(0);
}







/*---Temporisation, sert toujours...---------------------------------*/

void tempo(long temps)
{
   long i;
   for (i=0;i<temps;++i);
}

/*----Attend 1 appui sur <entree>--------------------------------------*/

void wait(void)
{

   char c;

   c=getc(stdin);
   c++;
}

/*---Attend le scan-code RETURN------------------------------------*/

void scanwait()

{
	while (iskey(SC_RETURN)==0);
}

/*---Renvoie true si pas d'appui, avec attente etc...------------------*/

int nokey(int k)

{
   if (iskey(k))
	{
	while (iskey(k));
	return(0);
	}
   else
	return(1);
}


int noesc(void)

{
	return(nokey(SC_ESC));
}

int noenter(void)

{
	return(nokey(SC_RETURN));
}


/*--Attend que le clavier soit libre ----------------------------------*/

void waitnokey(void)

{
	BYTE boolkey;
	int i;

	do
		{
		boolkey=0;
		for (i=1;i<128;++i)
			boolkey=(boolkey||iskey(i));
		}
	while
		(boolkey);
}

/*--Renvoie le numero d'une touche appuy�e---------------------*/

BYTE givekey(void)

{
	BYTE numkey=0;
	int i;

	waitnokey();

	do
		{
		numkey=0;
		for (i=1;i<128;++i)
			numkey=(iskey(i)) ? i : numkey;
		}

	while
		(numkey==0);

	return(numkey);
}

/*---Renvoie le numero de la premiere touche appuy�e, avec attente-----*/

BYTE firstkey(void)

{
	waitnokey();
	return(givekey());
}


/*----D�finition (tr�s) primaire des touches des curseurs---------------*/

void defkeycursor(void)

{


}

/*---- Bouge les curseurs -----------------------------------------------*/

void movecursor(void)

{
	int i;
	int x,y;


	for (i=0;i<MAXCURSOR;++i)
		if (cursor[i].actif)
			{
			x=cursor[i].x;
			y=cursor[i].y;


			if (hasbeenkey(cursor[i].up))
			 {
			 if (hasbeenkey(cursor[i].right))
			  {

			  if (cangoto(x+1,y-1))
			   {
			   cursor[i].x++;
			   cursor[i].y--;
			   }
			  else
			   {
			   if (cangoto(x,y-1))
			    cursor[i].y--;
			   else
			    {
			    if (cangoto(x+1,y))
			     cursor[i].x++;
			    }
			   }

			  }
			 else
			  {
			  if (hasbeenkey(cursor[i].left))
			   {

			   if (cangoto(x-1,y-1))
			    {
			    cursor[i].x--;
			    cursor[i].y--;
			    }
			   else
			    {
			    if (cangoto(x,y-1))
			     cursor[i].y--;
			    else
			     {
			     if (cangoto(x-1,y))
			      cursor[i].x--;
			     }
			    }

			   }
			  else
			   {

			   if (cangoto(x,y-1))
			    cursor[i].y--;
			   else
			    {
			    if (cangoto(x-1,y-1))
			     {
			     cursor[i].x--;
			     cursor[i].y--;
			     }
			    else
			     {
			     if (cangoto(x+1,y-1))
			      {
			      cursor[i].x++;
			      cursor[i].y--;
			      }
			     }
			    }

			   }
			  }
			 }
			else
			 {
			 if (hasbeenkey(cursor[i].down))
			  {
			  if (hasbeenkey(cursor[i].right))
			   {

			   if (cangoto(x+1,y+1))
			    {
			    cursor[i].x++;
			    cursor[i].y++;
			    }
			   else
			    {
			    if (cangoto(x,y+1))
			     cursor[i].y++;
			    else
			     {
			     if (cangoto(x+1,y))
			      cursor[i].x++;
			     }
			    }

			   }
			  else
			   {
			   if (hasbeenkey(cursor[i].left))
			    {

			    if (cangoto(x-1,y+1))
			     {
			     cursor[i].x--;
			     cursor[i].y++;
			     }
			    else
			     {
			     if (cangoto(x,y+1))
			      cursor[i].y++;
			     else
			      {
			      if (cangoto(x-1,y))
			       cursor[i].x--;
			      }
			     }

			    }
			   else
			    {

			    if (cangoto(x,y+1))
			     cursor[i].y++;
			    else
			     {
			     if (cangoto(x-1,y+1))
			      {
			      cursor[i].x--;
			      cursor[i].y++;
			      }
			     else
			      {
			      if (cangoto(x+1,y+1))
			       {
			       cursor[i].x++;
			       cursor[i].y++;
			       }
			      }
			     }

			    }
			   }
			  }
			  else
			  {
			  if (hasbeenkey(cursor[i].right))
			   {

			   if (cangoto(x+1,y))
			    cursor[i].x++;
			   else
			    {
			    if (cangoto(x+1,y-1))
			     {
			     cursor[i].x++;
			     cursor[i].y--;
			     }
			    else
			     {
			     if (cangoto(x+1,y+1))
			      {
			      cursor[i].x++;
			      cursor[i].y++;
			      }
			     }
			    }

			   }
			  else
			   {
			   if (hasbeenkey(cursor[i].left))
			    {

			    if (cangoto(x-1,y))
			     cursor[i].x--;
			    else
			     {
			     if (cangoto(x-1,y-1))
			      {
			      cursor[i].x--;
			      cursor[i].y--;
			      }
			     else
			      {
			      if (cangoto(x-1,y+1))
			       {
			       cursor[i].x--;
			       cursor[i].y++;
			       }
			      }
			     }

			    }
			   }
			  }
			 }

			if (cursor[i].x<1) cursor[i].x=1;
			if (cursor[i].x>sizex-2) cursor[i].x=sizex-2;
			if (cursor[i].y<1) cursor[i].y=1;
			if (cursor[i].y>sizey-2) cursor[i].y=sizey-2;

			pokegradient(cursor[i].val,
				     cursor[i].y*sizex+cursor[i].x,
				     cursor[i].color,
				     bigtab);
			};

}