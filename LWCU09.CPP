/* utilitaires de Liquid War */

#include "lwhd09.h"

#include <math.h>
#include <malloc.h>
#include <stdlib.h>
#include <dos.h>
#include <stdio.h>
#include <conio.h>


/*- minimum & maximum entier--------------------------*/

int mini(int a,int b)

{
	return((a<b) ? a : b);
}

int maxi(int a,int b)

{
	return((a>b) ? a : b);
}


/*----Cosinus entier -----------------*/

long cosinus(int angle)
{
 return(*(tabcos+(((unsigned int) angle)>>6)));
}



/*-----Sinus entier-----------------------------------------------------*/

long sinus(int angle)
{
 return(*(tabsin+(((unsigned int) angle)>>6)));
}



/*---allocation de m�moire align�e sur un segment--------------------*/

void far *coolfarmalloc(unsigned long n)

{
    void far *ptr;

    if ((ptr=farmalloc(n+15))==NULL)
       exit(1);
    return(alsegoff(ptr));
}



/*--Allocation des segments dans le heap-----------------------------*/

void allocseg(void)

{

  tabcos=(long far *) farmalloc(4096);
  tabsin=(long far *) farmalloc(4096);
  bigtab=(ALLIN16 far *) coolfarmalloc(262144);

}




/*--Rempli la (les) table(s) de valeurs----------------------------*/

void filltabval(void)

{
 unsigned int i;


 for (i=0;i<1024;++i)
   {
     *(tabcos+i)=(long) 65536*cos((M_PI*(i<<6))/32768);
     *(tabsin+i)=(long) 65536*sin((M_PI*(i<<6))/32768);
   }

}









/*---v�rification de l'allocation m�moire----*/

void	seeseg(void)

{
   printf("bigtab: %lX\n",bigtab);
}






/*-no comment--------------------------------*/

void truc(void)

{

	textmode(C80);

	clrscr();
	printf("Salut ThomThom!\n");
	tempo(10000000);
	printf("Comment vas-tu-yau de poil?\n");
	tempo(10000000);
	printf("Ca c'est de la vraie initialisation de la mort qui tue.\n");
	tempo(10000000);
	printf("J'esp�re que tu aimes mon inhibation clavier...\n");
	tempo(10000000);
	printf("Mais passons aux choses s�rieuses sans plus tarder!\n");
	tempo(10000000);
	clrscr();


}


/*---affiche les "scores" par couleur------------*/

void disphowmany(void)

{
	int i;

	for (i=0;i<6;++i)
		printf("Color %d : %d left\n",i,stillhowmany(i));


/*        int c;

	while ((c=getch())!=9) printf("%d\n",c);
*/
}

/*----- menu de la mort ---------------------------------*/

int vasymimile(void)

{
	textmode(C80);
	clrscr();

	printf("SUPER menu de Liquid War 2.9999+0.0001 (on y est presque)...\n\n\n");
	printf("Vas-y-Mimile, fais ton choix!\n\n");
	printf("1-Jouer (bon choix)\n");
	printf("2-Changer les SETTINGS (choix utile)\n");
	printf("3-Bidouiller le fichier des tableaux (choix foireux)\n");
	printf("4-Pas jouer (choix stupide)\n");

	return(getch());
}

/*------ choix primitif----------------------------------*/

void theprog(void)

{
	int i,c;
	int sortie;



	allocseg();
	initpal();
	init320200();
	for (i=0;i<4;++i)
		erasepage(i);
	setpage(3);
	loaddecor();
	setpalpic();
	showpage(3);

	filltabval();
	fillgoto();

	setpage(1);
	loadset();
	choseindir();
	setpage(2);
	setcadre();
	showpage(0);
	setpaltab();


	while (1)
		{
		setpage(2);
		showpage(2);

		if (choixduroi())
			break;

		setpalpic();
		showpage(3);

		randomcursor();
		putmenenhanced(20);

		erasepage(0);
		erasepage(1);

		showpage(0);
		setpaltab();

		runscanmode();

		while ((sortie=noesc())&&noenter())
			{
			setpage(i%2);
			movecursor();
			resetkeys();
			movesquare();
			refreshallgradient();
			dispallsquare();
			dispallcursor(i);
			showpage(i%2);
			++i;
			i=i%256;
			};

		leavescanmode();

		if (!sortie)
			break;
	};

	OutVga();

	textmode(C80);
	clrscr();

	printf("U-FOOT vous signale que le fond de l'air est frais.\n");

}
